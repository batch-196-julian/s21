// console.log("hello")

// Arrays - are used to store multiple realated data/values in a single variable.

let hobbies = ["Basketball", "Badminton", "Rides"];
// console.log(hobbies);

console.log(typeof hobbies);


let grades = [75.4,98.5,90.12,91.50]
const planets = ["Mercury", "Venus", "Mars", "Earth"];
console.log(grades);

let arraySample = ["Saitama", "One Punch Man", 2500, true];
console.log(arraySample);

// Best practice is Array must have same data type.

let dailyRoutine = ["Wake-up", "Breakfast", "Lunch", "Dinner", "Sleep"];
console.log(dailyRoutine);

let cities = ["Manila", "Tokyo", "Beijing", "Seoul"];
console.log(cities);


// Each item in an array is called an element

let username1 = "fighter_smith1";
let username2 = "josh@03";
let username3 = "white_knight";

let guildMembers = [username1,username2,username3];

console.log(guildMembers);

// .lenght property of an array tells about the number of elements in the array

console.log(dailyRoutine.length);
console.log(cities.length);

let fullName = "Randy Orton";
console.log(fullName.length);


dailyRoutine.lenght = dailyRoutine.length-1;
console.log(dailyRoutine.length);
console.log(dailyRoutine);

cities.length++;
console.log(cities);


fullName.length = fullName.length-1;
console.log(fullName);

let theBeatles = ["JOhn", "Paul", "Ringo", "George"];
theBeatles.length = theBeatles.length-1;
console.log(theBeatles);

// Accessing the elements of an Array - is one of the more common that we do with an array.

console.log(cities);
console.log(cities[0]);


lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegend);
console.log(lakersLegend[1]);
console.log(lakersLegend[3]);
lakersLegend.reverse();
console.log(lakersLegend);

lakersLegend[2]= "Pau Gasul";
console.log(lakersLegend);

let favoriteFoods = [

"Tonkatsu",
"Adobo",
"Hamburger",
"Sinigang",
"Pizza"

];

favoriteFoods[3] = "Tinolang Manok";
favoriteFoods[4] = "Barbeque";
console.log(favoriteFoods);

console.log(favoriteFoods[favoriteFoods.length-1]);



let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Scalabrine",];
console.log(bullsLegend[bullsLegend.length-1]);

let newArr = [];

console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);


for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index])
}

let numArr = [5,12,30,46,40];

for(let index = 0; index < numArr.length; index++){
	if (numArr[index] % 5 ===0) {
	console.log(numArr[index] + " is divisible by 5.")
	}else {
		console.log(numArr[index] + " is not divisible by 5.")
	}
}

let chessBoard = [

  ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

console.log(chessBoard[1][chessBoard.length-3]);

console.log(chessBoard[7][0]);
console.log(chessBoard[5][chessBoard.length-1]);